//
//  ViewController.swift
//  Locksmith Demo
//
//  Created by Edy Cu Tjong on 6/11/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit
import Locksmith

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Saving some data into keychain
        do {
            try Locksmith.saveData(data: ["password":"thisispassword"], forUserAccount: "LocksmithDemo")
        }
        catch {
            // Could not save the data into keychain
        }
        
        // Retrieve the data from keychain
        let dictionary = Locksmith.loadDataForUserAccount(userAccount: "LocksmithDemo")
        print(dictionary!)
    }


}

